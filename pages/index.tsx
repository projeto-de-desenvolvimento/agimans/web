import React, { useEffect, useState } from 'react'

import { useMutation, useQuery } from '@apollo/react-hooks'

import CardList from '../shared/components/Card/CardList'
import Form, { buttonTypes, formHTMLElement } from '../shared/components/Form'
import MainAgimans from '../shared/components/Main/MainAgimans'
import SubHeader from '../shared/components/SubHeader/SubHeader'
import SubHeaderLink from '../shared/components/SubHeader/SubHeaderLink'

import { AgimansCookie } from '../shared/utils/cookies'
import { cardTypes } from '../shared/components/Card/Card'
import { Container } from '../shared/components/common/styled'
import { ToastError, ToastSuccess } from '../shared/utils/Toast'
import { withApollo } from '../apollo/client'

import { ADD_GROUPS_TO_USER } from '../features/User/graphql/addGroupsToUser'
import { CREATE_GROUP } from '../features/Group/graphql/createGroup'
import { GET_USER_BY_EMAIL } from '../features/User/graphql/getUserByEmail'

interface Props {
  agimansCookie?: AgimansCookie
}

const Index = ({ agimansCookie }: Props) => {
  const [groups, setGroups] = useState([])

  const [addGroupsToUserMutation] = useMutation(ADD_GROUPS_TO_USER)
  const [createGroupMutation] = useMutation(CREATE_GROUP)

  const getUserByEmail = useQuery(GET_USER_BY_EMAIL, {
    variables: { email: agimansCookie?.userCookie?.email },
  })

  useEffect(() => {
    if (!getUserByEmail?.loading) {
      setGroups(getUserByEmail?.data?.users?.edges[0]?.node?.groups?.edges)
    }
  }, [getUserByEmail.loading])

  async function handleSubmit(event: any) {
    event.preventDefault()

    const nameElement = String(event.currentTarget.elements.name.value)
    const descriptionElement = String(event.currentTarget.elements.description.value)
    const projectElement = Boolean(event.currentTarget.elements.project.checked)
    const slugElement = String(event.currentTarget.elements.slug.value)

    try {
      const createGroup = await createGroupMutation({
        variables: {
          project: projectElement,
          name: nameElement,
          slug: slugElement,
          description: descriptionElement,
        },
      })

      const addGroupsToUser = await addGroupsToUserMutation({
        variables: {
          userId: getUserByEmail.data.users.edges[0].node.id,
          groupId: [createGroup.data.createOneGroup.id],
        },
      })

      setGroups(addGroupsToUser.data.addGroupsToUser.groups.edges.map((edge: any) => edge))
      ToastSuccess(
        `${projectElement ? 'Projeto' : 'Grupo'} ${createGroup.data.createOneGroup.name} criado com sucesso!`,
      )

      formHTMLElement.reset()
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <MainAgimans showHeader={true}>
      <SubHeader>
        <SubHeaderLink path="/" name="Meus Grupos e Projetos" />
      </SubHeader>
      <Container>
        <CardList className="grid-cards" cards={groups} type={cardTypes.groupOrProject} />
        <Form
          handleSubmit={handleSubmit}
          title="Criar Grupo ou Projeto"
          inputs={[
            { id: 'name', name: 'name', type: 'name', autoComplete: 'name', required: true, placeholder: 'Nome...' },
            {
              id: 'description',
              name: 'description',
              type: 'text',
              autoComplete: 'description',
              required: false,
              placeholder: 'Descrição...',
            },
            { id: 'slug', name: 'slug', type: 'text', autoComplete: 'slug', required: true, placeholder: 'URL...' },
            {
              id: 'project',
              name: 'project',
              type: 'checkbox',
              placeholder: 'É um Projeto?',
            },
          ]}
          buttons={[
            { name: 'Criar Grupo ou Projeto', type: buttonTypes.submit },
            { name: 'Cancelar', type: buttonTypes.reset },
          ]}
        />
      </Container>
    </MainAgimans>
  )
}

export default withApollo(Index)
