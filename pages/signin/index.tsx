import React, { useState } from 'react'
import router from 'next/router'

import { useMutation, useApolloClient } from '@apollo/react-hooks'

import Form, { buttonTypes } from '../../shared/components/Form'
import MainAgimans from '../../shared/components/Main/MainAgimans'
import SignInHeader from '../../shared/components/Sign/SignInHeader'

import { Left, Right, SignInBackground } from '../../shared/components/common/styled'
import { setAgimansCookie } from '../../shared/utils/cookies'
import { ToastError, ToastSuccess } from '../../shared/utils/Toast'

import { SIGN_IN } from '../../features/SignIn/graphql/signin'
import { SIGN_UP } from '../../features/SignUp/graphql/signup'

function SignIn() {
  const client = useApolloClient()

  const [hideSignIn, setHideSignIn] = useState(false)
  const [hideSignUp, setHideSignUp] = useState(true)

  const [signIn] = useMutation(SIGN_IN)
  const [signUp] = useMutation(SIGN_UP)

  function hideSignInForm() {
    setHideSignUp(false)
    setHideSignIn(true)
  }

  function hideSignUpForm() {
    setHideSignIn(false)
    setHideSignUp(true)
  }

  async function handleSubmitSignUp(event: any) {
    event.preventDefault()

    const nameElement = String(event.currentTarget.elements.name.value)
    const emailElement = String(event.currentTarget.elements.email.value)
    const passwordElement = String(event.currentTarget.elements.password.value)
    const confirmPasswordElement = String(event.currentTarget.elements.confirm_password.value)

    try {
      const { data } = await signUp({
        variables: {
          name: nameElement,
          email: emailElement,
          password: passwordElement,
          confirmPassword: confirmPasswordElement,
        },
      })

      ToastSuccess(`${data.createOneUser.name}, seu usuário foi criado com sucesso! Já podes logar.`)

      hideSignUpForm()
    } catch (error) {
      ToastError(error)
    }
  }

  async function handleSubmitSignIn(event: any) {
    event.preventDefault()

    const email = String(event.currentTarget.elements.email.value)
    const password = String(event.currentTarget.elements.password.value)

    try {
      await client.resetStore()

      const { data } = await signIn({
        variables: {
          email: email,
          password: password,
        },
      })

      setAgimansCookie({ token: data.login.token, email: data.login.email, name: data.login.name })

      ToastSuccess(`${data.login.name}, login feito com sucesso!`)

      router.push('/')
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <MainAgimans showHeader={false}>
      <SignInBackground>
        <Left />
        <Right>
          <SignInHeader />
          <Form
            hide={hideSignIn}
            signForm
            handleSubmit={handleSubmitSignIn}
            inputs={[
              { name: 'email', type: 'email', autoComplete: 'email', required: true, placeholder: 'E-mail...' },
              { name: 'password', type: 'password', autoComplete: 'password', required: true, placeholder: 'Senha...' },
            ]}
            buttons={[{ name: 'Logar', type: buttonTypes.submit }]}
            linkButtons={[{ name: 'Cadastrar', onClick: () => hideSignInForm() }]}
          />
          <Form
            hide={hideSignUp}
            signForm
            handleSubmit={handleSubmitSignUp}
            inputs={[
              { name: 'name', type: 'name', autoComplete: 'name', required: true, placeholder: 'Nome...' },
              { name: 'email', type: 'email', autoComplete: 'email', required: true, placeholder: 'E-mail...' },
              { name: 'password', type: 'password', autoComplete: 'password', required: true, placeholder: 'Senha...' },
              {
                name: 'confirm_password',
                type: 'password',
                autoComplete: 'confirm_password',
                required: true,
                placeholder: 'Confirmar Senha...',
              },
            ]}
            buttons={[{ name: 'Cadastrar', type: buttonTypes.submit }]}
            linkButtons={[{ name: 'Logar', onClick: () => hideSignUpForm() }]}
          />
        </Right>
      </SignInBackground>
    </MainAgimans>
  )
}

export default SignIn
