import React, { useEffect, useState } from 'react'

import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/router'

import CardList from '../../../shared/components/Card/CardList'
import FormSprint from './forms/FormSprint'
import FormStatus from './forms/FormStatus'
import MainAgimans from '../../../shared/components/Main/MainAgimans'
import Modal from '../../../shared/components/Modal'
import SidebarProject from '../../../shared/components/Sidebar/SidebarProject'
import SubHeader from '../../../shared/components/SubHeader/SubHeader'
import SubHeaderLink from '../../../shared/components/SubHeader/SubHeaderLink'

import { AddButton, Container, FiltersContainer } from '../../../shared/components/common/styled'
import { AgimansCookie } from '../../../shared/utils/cookies'
import { cardTypes } from '../../../shared/components/Card/Card'

import { GET_PROJECT_BY_SLUG } from '../../../features/Project/graphql/getProjectBySlug'
import { GET_USER_BY_EMAIL } from '../../../features/User/graphql/getUserByEmail'

interface Props {
  agimansCookie?: AgimansCookie
}

function SlugProject({ agimansCookie }: Props) {
  const [project, setProject] = useState({ id: null, name: '', slug: '' })
  const [sprints, setSprints] = useState([])
  const [status, setStatus] = useState([])

  const [showSprintForm, setShowSprintForm] = useState(false)
  const [showStatusForm, setShowStatusForm] = useState(false)

  const router = useRouter()
  const routerQuery = router.query

  const getProjectBySlug = useQuery(GET_PROJECT_BY_SLUG, {
    variables: { slug: routerQuery.slugProject },
  })
  const getUserByEmail = useQuery(GET_USER_BY_EMAIL, {
    variables: { email: agimansCookie.userCookie.email },
  })

  useEffect(() => {
    if (!getProjectBySlug?.loading) {
      setSprints(getProjectBySlug.data.groups.edges[0].node.sprints.edges)
      setStatus(getProjectBySlug.data.groups.edges[0].node.statuses.edges)
      setProject(getProjectBySlug.data.groups.edges[0].node)
    }
  }, [getProjectBySlug.loading, getUserByEmail.loading])

  return (
    <MainAgimans sidebarChildren={<SidebarProject slugProject={project.slug} />} showHeader>
      <SubHeader>
        <SubHeaderLink path={`/project/[slugProject]`} as={`/project/${project.slug}`} name={project.name} />
      </SubHeader>
      <FiltersContainer>
        <div></div>
        <div className="button-container">
          <div className="add-button-container label">
            <AddButton className="label" onClick={() => setShowStatusForm(true)}>
              <p>Status</p>
              <img src="/assets/icons/add-button-plus.svg" />
            </AddButton>
          </div>
          <div className="add-button-container label">
            <AddButton className="label" onClick={() => setShowSprintForm(true)}>
              <p>Sprint</p>
              <img src="/assets/icons/add-button-plus.svg" />
            </AddButton>
          </div>
        </div>
      </FiltersContainer>
      <Container>
        <CardList className="grid-cards" cards={sprints} type={cardTypes.sprint} />
        <CardList className="status" cards={status} type={cardTypes.status} />
        <Modal showModal={showSprintForm}>
          <FormSprint project={project} setSprints={setSprints} setShowSprintForm={setShowSprintForm} />
        </Modal>
        <Modal showModal={showStatusForm}>
          <FormStatus project={project} setStatus={setStatus} setShowStatusForm={setShowStatusForm} />
        </Modal>
      </Container>
    </MainAgimans>
  )
}

export default SlugProject
