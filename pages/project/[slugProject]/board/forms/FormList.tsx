import React from 'react'

import { useMutation } from '@apollo/react-hooks'

import Form, { buttonTypes } from '../../../../../shared/components/Form'

import { ToastError, ToastSuccess } from '../../../../../shared/utils/Toast'

import { CREATE_LIST } from '../../../../../features/Project/List/graphql/createList'
import { SET_SPRINT_ON_LIST } from '../../../../../features/Project/List/graphql/setSprintOnList'
import { SET_STATUS_ON_LIST } from '../../../../../features/Project/List/graphql/setStatusOnList'

interface Props {
  setLists: any
  setShowListForm: any
  sprintId: any
  status: any
}

export default function FormList({ setLists, setShowListForm, sprintId, status }: Props) {
  const [createListMutation] = useMutation(CREATE_LIST)
  const [setSprintOnListMutation] = useMutation(SET_SPRINT_ON_LIST)
  const [setStatusOnListMutation] = useMutation(SET_STATUS_ON_LIST)

  async function handleSubmitList(event: any) {
    event.preventDefault()

    const nameElement = String(event.currentTarget.elements.name.value)
    const descriptionElement = String(event.currentTarget.elements.description.value)
    const selectStatusElement = Number(event.currentTarget.elements.selectStatus.value)

    try {
      const createList = await createListMutation({
        variables: {
          name: nameElement,
          description: descriptionElement,
        },
      })

      await setStatusOnListMutation({
        variables: {
          statusId: selectStatusElement,
          listId: createList.data.createOneList.id,
        },
      })

      const setSprintOnList = await setSprintOnListMutation({
        variables: { listId: createList.data.createOneList.id, sprintId: sprintId },
      })

      setLists(setSprintOnList.data.setSprintOnList.sprint.lists.edges)

      ToastSuccess(`Lista ${createList.data.createOneList.name} criada com sucesso!`)
      setShowListForm(false)
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <Form
      handleSubmit={handleSubmitList}
      title="Criar Lista"
      inputs={[
        { id: 'name', name: 'name', type: 'text', autoComplete: 'name', required: true, placeholder: 'Nome...' },
        {
          id: 'description',
          name: 'description',
          type: 'textarea',
          autoComplete: 'description',
          required: true,
          placeholder: 'Descrição...',
        },
        {
          id: 'selectStatus',
          name: 'selectStatus',
          type: 'select',
          placeholder: 'Status',
          options: status,
        },
      ]}
      buttons={[
        { type: buttonTypes.submit, name: 'Criar Lista' },
        { type: buttonTypes.reset, onClick: () => setShowListForm(false), name: 'Cancelar' },
      ]}
    />
  )
}
