import React from 'react'

import { useMutation } from '@apollo/react-hooks'

import Form, { buttonTypes } from '../../../../../shared/components/Form'

import { ToastError, ToastSuccess } from '../../../../../shared/utils/Toast'

import { CREATE_ISSUE } from '../../../../../features/Project/Issue/graphql/createIssue'
import { SET_SPRINT_LIST_AND_USER_TO_ISSUE } from '../../../../../features/Project/Issue/graphql/setListAndUser'

interface Props {
  list: any
  setLists: any
  setShowIssueForm: any
  sprintId: any
  userId: any
}

export default function FormCreateIssue({ list, setLists, setShowIssueForm, sprintId, userId }: Props) {
  const [createIssueMutation] = useMutation(CREATE_ISSUE)
  const [setListAndUserMutation] = useMutation(SET_SPRINT_LIST_AND_USER_TO_ISSUE)

  async function handleSubmitIssue(event: any) {
    event.preventDefault()

    const titleElement = String(event.currentTarget.elements.title.value)
    const descriptionElement = String(event.currentTarget.elements.description.value)
    const estimateElement = Number(event.currentTarget.elements.estimate.value)
    const spendElement = Number(event.currentTarget.elements.spend.value)
    const weightElement = Number(event.currentTarget.elements.weight.value)

    try {
      if (list) {
        const createIssue = await createIssueMutation({
          variables: {
            title: titleElement,
            description: descriptionElement,
            estimate: estimateElement,
            spend: spendElement,
            weight: weightElement,
          },
        })

        const setListAndUser = await setListAndUserMutation({
          variables: {
            issueId: createIssue.data.createOneIssue.id,
            listId: list.node.id,
            sprintId: sprintId,
            userId: userId,
          },
        })

        setLists(setListAndUser.data.setSprintOnIssue.sprint.lists.edges)
        ToastSuccess(`História ${createIssue.data.createOneIssue.title} criada com sucesso!`)
        setShowIssueForm(false)
      }
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <Form
      handleSubmit={handleSubmitIssue}
      title="Criar História na lista"
      subtitle={list.node.name}
      inputs={[
        { name: 'title', type: 'text', autoComplete: 'title', required: true, placeholder: 'Título...' },
        {
          name: 'description',
          type: 'textarea',
          autoComplete: 'description',
          required: true,
          placeholder: 'Descrição...',
        },
        { name: 'estimate', type: 'number', placeholder: 'Tempo Estimado (h)...' },
        { name: 'spend', type: 'number', placeholder: 'Tempo Trabalhado (h)...' },
        { name: 'weight', type: 'number', placeholder: 'Peso...' },
      ]}
      buttons={[
        { type: buttonTypes.submit, name: 'Criar História' },
        { type: buttonTypes.reset, onClick: () => setShowIssueForm(false), name: 'Cancelar' },
      ]}
    />
  )
}
