import React from 'react'

import { useMutation } from '@apollo/react-hooks'

import Form, { buttonTypes } from '../../../../../shared/components/Form'

import { ToastError, ToastSuccess } from '../../../../../shared/utils/Toast'

import { SET_SPRINT_AND_LIST_TO_ISSUE } from '../../../../../features/Project/Issue/graphql/setSprintAndList'
import { SET_SPRINT_LIST_AND_USER_TO_ISSUE } from '../../../../../features/Project/Issue/graphql/setListAndUser'
import { UPDATE_ISSUE } from '../../../../../features/Project/Issue/graphql/updateIssue'

interface Props {
  issueToUpdate: any
  setLists: any
  setIssueDetails: any
  setShowModalIssueUpdate: any
  sprintId: any
  userId: any
}

export default function FormUpdateIssue({
  issueToUpdate,
  setLists,
  setIssueDetails,
  setShowModalIssueUpdate,
  sprintId,
  userId,
}: Props) {
  const [setListAndUserMutation] = useMutation(SET_SPRINT_LIST_AND_USER_TO_ISSUE)
  const [setSprintAndListMutation] = useMutation(SET_SPRINT_AND_LIST_TO_ISSUE)
  const [updateIssueMutation] = useMutation(UPDATE_ISSUE)

  async function handleSubmitUpdateIssue(event: any) {
    event.preventDefault()

    const idElement = Number(issueToUpdate.id)
    const titleElement = String(event.target.title.attributes.value.value)
    const descriptionElement = String(event.target.description.value)
    const estimateElement = Number(event.currentTarget.elements.estimate.value)
    const spendElement = Number(event.currentTarget.elements.spend.value)
    const weightElement = Number(event.currentTarget.elements.weight.value)

    try {
      const updateIssue = await updateIssueMutation({
        variables: {
          id: idElement,
          title: titleElement,
          description: descriptionElement,
          estimate: estimateElement,
          spend: spendElement,
          weight: weightElement,
        },
      })

      const listId = updateIssue.data.updateOneIssue.users.edges.map((listIdNode) => listIdNode.node.id)

      if (listId.includes(userId)) {
        const setSprintAndList = await setSprintAndListMutation({
          variables: {
            issueId: updateIssue.data.updateOneIssue.id,
            listId: updateIssue.data.updateOneIssue.list.id,
            sprintId: sprintId,
          },
        })

        setLists(setSprintAndList.data.setSprintOnIssue.sprint.lists.edges)
      } else {
        const setListAndUser = await setListAndUserMutation({
          variables: {
            issueId: updateIssue.data.updateOneIssue.id,
            listId: updateIssue.data.updateOneIssue.list.id,
            sprintId: sprintId,
            userId: userId,
          },
        })

        setLists(setListAndUser.data.setSprintOnIssue.sprint.lists.edges)
      }

      setIssueDetails({
        id: updateIssue.data.updateOneIssue.id,
        title: updateIssue.data.updateOneIssue.title,
        description: updateIssue.data.updateOneIssue.description,
        estimate: updateIssue.data.updateOneIssue.estimate,
        spend: updateIssue.data.updateOneIssue.spend,
        weight: updateIssue.data.updateOneIssue.weight,
      })

      ToastSuccess(`História ${updateIssue.data.updateOneIssue.title} editada com sucesso!`)
      setShowModalIssueUpdate(false)
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <Form
      handleSubmit={handleSubmitUpdateIssue}
      title="Editar História na lista"
      inputs={[
        {
          name: 'title',
          type: 'text',
          autoComplete: 'title',
          required: true,
          placeholder: 'Título...',
          defaultValue: issueToUpdate.title,
          onChange: issueToUpdate.title,
        },
        {
          name: 'description',
          type: 'textarea',
          autoComplete: 'description',
          required: true,
          placeholder: 'Descrição...',
          defaultValue: issueToUpdate.description,
          onChange: issueToUpdate.description,
        },
        {
          name: 'estimate',
          type: 'number',
          placeholder: 'Tempo Estimado (h)...',
          defaultValue: issueToUpdate.estimate,
          onChange: issueToUpdate.estimate,
        },
        {
          name: 'spend',
          type: 'number',
          placeholder: 'Tempo Trabalhado (h)...',
          defaultValue: issueToUpdate.spend,
          onChange: issueToUpdate.spend,
        },
        {
          name: 'weight',
          type: 'number',
          placeholder: 'Peso...',
          defaultValue: issueToUpdate.weight,
          onChange: issueToUpdate.weight,
        },
      ]}
      buttons={[
        { type: buttonTypes.submit, name: 'Editar História' },
        { type: buttonTypes.reset, onClick: () => setShowModalIssueUpdate(false), name: 'Cancelar' },
      ]}
    />
  )
}
