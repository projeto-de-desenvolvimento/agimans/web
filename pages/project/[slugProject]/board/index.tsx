import React, { useEffect, useState } from 'react'
import produce from 'immer'

import { useMutation, useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/router'

import BoardContext from './context'
import FormCreateIssue from './forms/FormCreateIssue'
import FormIssueDetails from './forms/FormIssueDetails'
import FormList from './forms/FormList'
import FormUpdateIssue from './forms/FormUpdateIssue'
import List from '../../../../shared/components/List/List'
import MainAgimans from '../../../../shared/components/Main/MainAgimans'
import Modal from '../../../../shared/components/Modal'
import SidebarProject from '../../../../shared/components/Sidebar/SidebarProject'
import SubHeader from '../../../../shared/components/SubHeader/SubHeader'
import SubHeaderLink from '../../../../shared/components/SubHeader/SubHeaderLink'

import { AddButton, BoardContainer, FiltersContainer, StyledSelect } from '../../../../shared/components/common/styled'
import { AgimansCookie } from '../../../../shared/utils/cookies'

import { GET_LISTS_FROM_SPRINT } from '../../../../features/Project/Sprint/graphql/getListsFromSprint'
import { GET_PROJECT_BY_SLUG } from '../../../../features/Project/graphql/getProjectBySlug'
import { GET_USER_BY_EMAIL } from '../../../../features/User/graphql/getUserByEmail'
import { UPDATE_ISSUE_INDEX } from '../../../../features/Project/Issue/graphql/updateIssueIndex'

interface Props {
  agimansCookie?: AgimansCookie
}

export interface IssueDetailsInterface {
  id?: number
  title?: string
  description?: string
  estimate?: number
  spend?: number
  weight?: number
}

function Board({ agimansCookie }: Props) {
  const [issueDetails, setIssueDetails] = useState({
    id: 0,
    title: '',
    description: '',
    estimate: 0,
    spend: 0,
    weight: 0,
  })
  const [issueToUpdate, setIssueToUpdate] = useState()
  const [list, setList] = useState()
  const [lists, setLists] = useState([])
  const [project, setProject] = useState({ name: '', slug: '' })
  const [showIssueForm, setShowIssueForm] = useState(false)
  const [showIssueModal, setShowIssueModal] = useState(false)
  const [showListForm, setShowListForm] = useState(false)
  const [showModalIssueUpdate, setShowModalIssueUpdate] = useState(false)
  const [sprintId, setSprintId] = useState('')
  const [sprints, setSprints] = useState([])
  const [status, setStatus] = useState([])
  const [userId, setUserId] = useState(0)

  const [updateIssueIndexMutation] = useMutation(UPDATE_ISSUE_INDEX)

  const router = useRouter()
  const routerQuery = router.query

  const getProjectBySlug = useQuery(GET_PROJECT_BY_SLUG, {
    variables: { slug: routerQuery.slugProject },
  })

  const getUserByEmail = useQuery(GET_USER_BY_EMAIL, {
    variables: { email: agimansCookie.userCookie.email },
  })

  useEffect(() => {
    if (!getUserByEmail.loading) setUserId(getUserByEmail.data.users.edges[0].node.id)

    if (!getProjectBySlug?.loading) {
      setProject(getProjectBySlug.data.groups.edges[0].node)
      setSprints(getProjectBySlug.data.groups.edges[0].node.sprints.edges)
      setStatus(getProjectBySlug.data.groups.edges[0].node.statuses.edges)
    }
  }, [getProjectBySlug.loading, getUserByEmail.loading])

  useEffect(() => {
    const currentSprint = sprints.filter((sprint) => sprint.node.id === Number(sprintId))[0]
    setLists(currentSprint?.node?.lists?.edges)
  }, [sprintId])

  function changeSprint() {
    const select = document.getElementById('select-sprint')
    const value = select.options[select.selectedIndex].value

    setSprintId(value)
  }

  function moveToList(fromList: number, toList: number, from: number, to: number) {
    setLists(
      produce(lists, (draft) => {
        const dragged = draft[fromList].node.issues.edges[from]

        draft[fromList].node.issues.edges.splice(from, 1)
        draft[toList].node.issues.edges.splice(to, 0, dragged)

        draft[fromList].node.issues.edges.map(
          async (draftIssue: any, index: number) =>
            await updateIssueIndex(draftIssue.node.id, index, draft[fromList].node.id),
        )
        draft[toList].node.issues.edges.map(
          async (draftIssue: any, index: number) =>
            await updateIssueIndex(draftIssue.node.id, index, draft[toList].node.id),
        )
      }),
    )
  }

  function moveToCard(fromList: number, toList: number, from: number, to: number) {
    setLists(
      produce(lists, (draft) => {
        const dragged = draft[fromList].node.issues.edges[from]

        draft[fromList].node.issues.edges.splice(from, 1)
        draft[toList].node.issues.edges.splice(to, 0, dragged)

        if (fromList === toList) {
          draft[fromList].node.issues.edges.map(
            async (draftIssue: any, index: number) =>
              await updateIssueIndex(draftIssue.node.id, index, draft[fromList].node.id),
          )
        } else {
          draft[fromList].node.issues.edges.map(
            async (draftIssue: any, index: number) =>
              await updateIssueIndex(draftIssue.node.id, index, draft[fromList].node.id),
          )
          draft[toList].node.issues.edges.map(
            async (draftIssue: any, index: number) =>
              await updateIssueIndex(draftIssue.node.id, index, draft[toList].node.id),
          )
        }
      }),
    )
  }

  async function updateIssueIndex(issueId: number, indexIssue: number, listId: number) {
    await updateIssueIndexMutation({
      variables: {
        id: issueId,
        index: indexIssue,
        listId: listId,
      },
    })
  }

  function showFormModal(list: any) {
    setList(list)
    setShowIssueForm(true)
  }

  function showIssueDetails(details: IssueDetailsInterface) {
    if (showIssueModal) setShowIssueModal(false)
    setIssueDetails(details)
    setShowIssueModal(true)
  }

  function showModalIssueToUpdate(issue) {
    setIssueToUpdate(issue)
    setList(list)
    setShowModalIssueUpdate(true)
  }

  return (
    <MainAgimans sidebarChildren={<SidebarProject slugProject={project.slug} />} showHeader>
      <SubHeader>
        <SubHeaderLink path={`/project/[slugProject]`} as={`/project/${project.slug}`} name={project.name} />
        <SubHeaderLink path={`/project/[slugProject]/board`} as={`/project/${project.slug}/board`} name={'Board'} />
      </SubHeader>
      <FiltersContainer>
        <div className="styled-select-container">
          <StyledSelect id="select-sprint" onChange={() => changeSprint()}>
            <option value="">Sprints</option>
            {sprints.map((sprint) => (
              <option key={sprint.node.id} value={sprint.node.id}>
                {sprint.node.title}
              </option>
            ))}
          </StyledSelect>
        </div>
        <div className="add-button-container">
          <AddButton onClick={() => setShowListForm(true)}>
            <img src="/assets/icons/add-button-plus.svg" />
          </AddButton>
        </div>
      </FiltersContainer>
      <BoardContext.Provider value={{ lists: Array, moveToCard, moveToList }}>
        <Modal showModal={showListForm}>
          <FormList setLists={setLists} setShowListForm={setShowListForm} sprintId={sprintId} status={status} />
        </Modal>
        <Modal showModal={showIssueModal}>
          <FormIssueDetails
            issueDetails={issueDetails}
            showModalIssueToUpdate={showModalIssueToUpdate}
            setShowIssueModal={setShowIssueModal}
            userId={userId}
          />
        </Modal>
        {issueToUpdate && (
          <Modal showModal={showModalIssueUpdate}>
            <FormUpdateIssue
              issueToUpdate={issueToUpdate}
              setLists={setLists}
              setIssueDetails={setIssueDetails}
              setShowModalIssueUpdate={setShowModalIssueUpdate}
              sprintId={sprintId}
              userId={userId}
            />
          </Modal>
        )}
        {list && (
          <Modal showModal={showIssueForm}>
            <FormCreateIssue
              list={list}
              setLists={setLists}
              setShowIssueForm={setShowIssueForm}
              sprintId={sprintId}
              userId={userId}
            />
          </Modal>
        )}
        <BoardContainer>
          {lists?.map((list, index: number) => (
            <List
              key={list.node.id}
              name={list.node.name}
              childCards={list.node.issues.edges}
              index={index}
              statusColor={list?.node?.status?.color}
              callFunction={() => showFormModal(list)}
              callFuntionToCards={showIssueDetails}
            />
          ))}
        </BoardContainer>
      </BoardContext.Provider>
    </MainAgimans>
  )
}

export default Board
