import React, { useEffect, useState } from 'react'

import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis, YAxis } from 'recharts'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/router'

import MainAgimans from '../../../../shared/components/Main/MainAgimans'
import SidebarProject from '../../../../shared/components/Sidebar/SidebarProject'
import SubHeader from '../../../../shared/components/SubHeader/SubHeader'
import SubHeaderLink from '../../../../shared/components/SubHeader/SubHeaderLink'

import { AgimansCookie } from '../../../../shared/utils/cookies'

import { GET_ISSUES_AND_SPRINTS_FROM_PROJECT } from '../../../../features/Project/graphql/getIssuesAndSprintsFromProject'
import styled from 'styled-components'

interface Props {
  agimansCookie?: AgimansCookie
}

const GraphicsContainer = styled.div`
  display: grid;
  width: 100%;
  height: calc(100% - 50px);
  grid-template-columns: auto auto;
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  padding: 20px;

  > :first-child {
    box-shadow: 0 0 black;
    background-color: transparent;
  }

  > div {
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #ffffff;
    box-shadow: 0px 0px 5px #757575;
    border-radius: 5px;
    flex-direction: column;

    h3 {
      margin: 10px 0;
      font-weight: 400;
      color: ${({ theme }) => theme.font.medium};
    }
  }
`

export default function Progress({ agimansCookie }: Props) {
  const [chartWidth, setChartWidth] = useState(0)
  const [chartHeight, setChartHeight] = useState(0)
  const [dataSprints, setDataSprints] = useState()
  const [project, setProject] = useState({ name: '', slug: '' })
  const [user, setUser] = useState()

  const data = [
    { name: 'Page A', uv: 400, pv: 2400, amt: 2400 },
    { name: 'Page B', uv: 400 / 3, pv: 2400 / 3, amt: 2400 / 3 },
    { name: 'Page C', uv: 400 / 2, pv: 2400 / 2, amt: 2400 / 2 },
  ]

  const router = useRouter()
  const routerQuery = router.query

  const getIssuesAndSprintsFromProject = useQuery(GET_ISSUES_AND_SPRINTS_FROM_PROJECT, {
    variables: {
      email: agimansCookie.userCookie.email,
      slug: routerQuery.slugProject,
    },
  })

  useEffect(() => {
    setChartWidth((document.documentElement.clientWidth - 200) / 2)
    setChartHeight((document.documentElement.clientHeight - 100) / 2 - 60 - 20)

    if (getIssuesAndSprintsFromProject?.data?.users?.edges[0]?.node) {
      setProject(getIssuesAndSprintsFromProject.data.users.edges[0].node.groups.edges[0].node)
      setUser(getIssuesAndSprintsFromProject.data.users.edges[0].node)

      const dataSprints = getIssuesAndSprintsFromProject.data.users.edges[0].node.groups.edges[0].node.sprints.edges.map(
        (nodeSprint) => {
          const retorno = { name: '', 'N° Issues': 0, Estimado: 0, Trabalhado: 0, Peso: 0 }

          const issues = nodeSprint.node.issues.edges.filter((issue) => {
            const retornos = issue.node.users.edges.map((user) =>
              user.node.email === agimansCookie.userCookie.email ? true : false,
            )

            console.log('issue.node', issue.node)

            if (retornos[0]) {
              retorno['Estimado'] += issue.node.estimate
              retorno['Trabalhado'] += issue.node.spend
              retorno['Peso'] += issue.node.weight

              return issue.node
            }
          })

          retorno.name = nodeSprint.node.title
          retorno['N° Issues'] = issues.length

          return retorno
        },
      )

      setDataSprints(dataSprints)
    }
  }, [getIssuesAndSprintsFromProject.loading])

  return (
    <MainAgimans sidebarChildren={<SidebarProject slugProject={project.slug} />} showHeader>
      <SubHeader>
        <SubHeaderLink path={`/project/[slugProject]`} as={`/project/${project.slug}`} name={project.name} />
        <SubHeaderLink
          path={`/project/[slugProject]/progress`}
          as={`/project/${project.slug}/progress`}
          name={'Progresso'}
        />
      </SubHeader>
      <GraphicsContainer>
        <div></div>
        <div>
          <h3>Número de Histórias</h3>
          <BarChart width={chartWidth} height={chartHeight} data={dataSprints}>
            <Tooltip />
            <XAxis dataKey="name" />
            <YAxis />
            <Bar dataKey="N° Issues" fill="#8884d8" />
          </BarChart>
        </div>
        <div>
          <h3>Estimado x Trabalho</h3>
          <BarChart width={chartWidth} height={chartHeight} data={dataSprints}>
            <Tooltip />
            <XAxis dataKey="name" />
            <YAxis />
            <Bar dataKey="Estimado" fill="#8884d8" />
            <Bar dataKey="Trabalhado" fill="#82ca9d" />
          </BarChart>
        </div>
        <div>
          <h3>Peso da Sprint</h3>
          <BarChart width={chartWidth} height={chartHeight} data={dataSprints}>
            <Tooltip />
            <XAxis dataKey="name" />
            <YAxis />
            <Bar dataKey="Peso" fill="#8884d8" />
          </BarChart>
        </div>
      </GraphicsContainer>
    </MainAgimans>
  )
}
