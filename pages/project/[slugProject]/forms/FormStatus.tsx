import React from 'react'

import { useMutation } from '@apollo/react-hooks'

import Form, { buttonTypes } from '../../../../shared/components/Form'

import { ToastError, ToastSuccess } from '../../../../shared/utils/Toast'

import { ADD_STATUS_TO_PROJECT } from '../../../../features/Project/graphql/addStatusToProject'
import { CREATE_STATUS } from '../../../../features/Project/Status/graphql/createStatus'

interface Props {
  project: any
  setStatus: any
  setShowStatusForm: any
}

export default function FormStatus({ project, setStatus, setShowStatusForm }: Props) {
  const [createStatusMutation] = useMutation(CREATE_STATUS)
  const [addStatusToProjectMutation] = useMutation(ADD_STATUS_TO_PROJECT)

  async function handleSubmitStatus(event: any) {
    event.preventDefault()

    const nameElement = String(event.currentTarget.elements.name.value)
    const colorElement = String(event.currentTarget.elements.color.value)

    try {
      const createStatus = await createStatusMutation({
        variables: {
          name: nameElement,
          color: colorElement,
        },
      })

      const addStatusToProject = await addStatusToProjectMutation({
        variables: {
          projectId: project.id,
          statusId: [createStatus.data.createOneStatus.id],
        },
      })

      setStatus(addStatusToProject.data.addStatusesToGroup.statuses.edges)

      ToastSuccess(`Status ${createStatus.data.createOneStatus.name} criado com sucesso!`)
      setShowStatusForm(false)
    } catch (error) {
      ToastError(error)
    }
  }

  return (
    <Form
      handleSubmit={handleSubmitStatus}
      title="Criar Status"
      inputs={[
        {
          id: 'name',
          name: 'name',
          type: 'text',
          autoComplete: 'name',
          required: true,
          placeholder: 'Nome...',
        },
        {
          id: 'color',
          name: 'color',
          type: 'color',
          autoComplete: 'color',
          required: true,
          label: 'Cor',
        },
      ]}
      buttons={[
        { type: buttonTypes.submit, name: 'Criar Status' },
        { type: buttonTypes.reset, onClick: () => setShowStatusForm(false), name: 'Cancelar' },
      ]}
    />
  )
}
