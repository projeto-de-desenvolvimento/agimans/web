module.exports = {
  serverRuntimeConfig: {
    API_URL: process.env.API_URL,
  },
  typescript: {
    ignoreBuildErrors: true,
  },
}
