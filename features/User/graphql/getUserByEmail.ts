import gql from 'graphql-tag'

export const GET_USER_BY_EMAIL = gql`
  query getUserByEmail($email: String!) {
    users(filter: { email: { eq: $email } }) {
      edges {
        node {
          id
          email
          name
          groups {
            edges {
              node {
                id
                project
                name
                slug
                description
              }
            }
          }
        }
      }
    }
  }
`
