import gql from 'graphql-tag'

export const GET_USER_GROUPS = gql`
  query getUserGroups($email: String!) {
    users(filter: { email: { eq: $email } }) {
      edges {
        node {
          email
          name
          groups {
            edges {
              node {
                project
                name
                slug
                description
                fatherGroup {
                  active
                  project
                  name
                  slug
                  description
                }
                childGroups {
                  edges {
                    node {
                      project
                      name
                      slug
                      description
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
