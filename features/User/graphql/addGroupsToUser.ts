import gql from 'graphql-tag'

export const ADD_GROUPS_TO_USER = gql`
  mutation addGroupsToUser($userId: ID!, $groupId: [ID!]!) {
    addGroupsToUser(input: { id: $userId, relationIds: $groupId }) {
      id
      email
      name
      groups {
        edges {
          node {
            id
            name
            slug
            description
            project
          }
        }
      }
    }
  }
`
