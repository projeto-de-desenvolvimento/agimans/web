import gql from 'graphql-tag'

export const SIGN_UP = gql`
  mutation signUp($email: String!, $name: String!, $password: String!, $confirmPassword: String!) {
    createOneUser(
      input: { user: { email: $email, name: $name, password: $password, confirmPassword: $confirmPassword } }
    ) {
      name
    }
  }
`
