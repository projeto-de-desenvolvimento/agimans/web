import gql from 'graphql-tag'

export const SET_SPRINT_ON_LIST = gql`
  mutation($listId: ID!, $sprintId: ID!) {
    setSprintOnList(input: { id: $listId, relationId: $sprintId }) {
      id
      active
      index
      name
      description
      createdAt
      updatedAt
      deletedAt
      sprint {
        active
        index
        endDate
        startDate
        title
        description
        estimate
        spend
        weight
        lists(sorting: [{ field: index, direction: ASC }]) {
          edges {
            node {
              id
              active
              index
              name
              description
              issues(sorting: [{ field: index, direction: ASC }]) {
                edges {
                  node {
                    id
                    active
                    index
                    title
                    description
                    estimate
                    spend
                    weight
                  }
                }
              }
              status {
                id
                active
                color
                name
              }
            }
          }
        }
      }
    }
  }
`
