import gql from 'graphql-tag'

export const ADD_SPRINT_TO_PROJECT = gql`
  mutation($projectId: ID!, $sprintId: [ID!]!) {
    addSprintsToGroup(input: { id: $projectId, relationIds: $sprintId }) {
      active
      name
      project
      slug
      description
      sprints {
        edges {
          node {
            id
            active
            endDate
            startDate
            title
            description
          }
        }
      }
    }
  }
`
