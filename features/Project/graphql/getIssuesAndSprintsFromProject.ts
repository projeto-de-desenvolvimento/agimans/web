import gql from 'graphql-tag'

export const GET_ISSUES_AND_SPRINTS_FROM_PROJECT = gql`
  query($email: String!, $slug: String!) {
    users(filter: { email: { eq: $email } }) {
      edges {
        node {
          id
          active
          email
          name
          groups(filter: { project: { is: true }, slug: { eq: $slug } }) {
            edges {
              node {
                id
                active
                project
                name
                slug
                description
                sprints {
                  edges {
                    node {
                      id
                      active
                      index
                      endDate
                      startDate
                      title
                      description
                      estimate
                      spend
                      weight
                      issues {
                        edges {
                          node {
                            id
                            active
                            index
                            title
                            description
                            estimate
                            spend
                            weight
                            users {
                              edges {
                                node {
                                  email
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
