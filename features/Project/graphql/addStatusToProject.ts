import gql from 'graphql-tag'

export const ADD_STATUS_TO_PROJECT = gql`
  mutation($projectId: ID!, $statusId: [ID!]!) {
    addStatusesToGroup(input: { id: $projectId, relationIds: $statusId }) {
      id
      active
      name
      project
      slug
      description
      createdAt
      updatedAt
      deletedAt
      statuses {
        edges {
          node {
            id
            active
            color
            name
          }
        }
      }
    }
  }
`
