import gql from 'graphql-tag'

export const UPDATE_ISSUE = gql`
  mutation($id: ID!, $title: String!, $description: String!, $estimate: Float, $spend: Float, $weight: Float) {
    updateOneIssue(
      input: {
        id: $id
        update: { title: $title, description: $description, estimate: $estimate, spend: $spend, weight: $weight }
      }
    ) {
      id
      active
      index
      title
      description
      estimate
      spend
      weight
      list {
        id
      }
      users {
        edges {
          node {
            id
          }
        }
      }
    }
  }
`
