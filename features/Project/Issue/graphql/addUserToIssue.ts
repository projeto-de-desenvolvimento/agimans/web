import gql from 'graphql-tag'

export const ADD_USER_TO_ISSUE = gql`
  mutation($issueId: ID!, $userId: ID!) {
    addUsersToIssue(input: { id: $issueId, relationIds: [$userId] }) {
      id
      active
      index
      title
      description
      estimate
      spend
      weight
      users {
        edges {
          node {
            id
            active
            email
            name
          }
        }
      }
    }
  }
`
