import gql from 'graphql-tag'

export const CREATE_STATUS = gql`
  mutation($color: String!, $name: String!) {
    createOneStatus(input: { status: { color: $color, name: $name } }) {
      id
      active
      color
      name
      createdAt
      updatedAt
      deletedAt
    }
  }
`
