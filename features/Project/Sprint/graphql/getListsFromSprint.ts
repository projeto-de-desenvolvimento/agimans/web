import gql from 'graphql-tag'

export const GET_LISTS_FROM_SPRINT = gql`
  query($id: ID!) {
    sprint(id: $id) {
      id
      active
      endDate
      startDate
      title
      description
      lists(sorting: [{ field: index, direction: ASC }]) {
        edges {
          node {
            # index
            id
            active
            name
            description
            issues(sorting: [{ field: index, direction: ASC }]) {
              edges {
                node {
                  # index
                  id
                  active
                  title
                  description
                  estimate
                  spend
                  weight
                }
              }
            }
            status {
              id
              active
              color
              name
            }
          }
        }
      }
    }
  }
`
