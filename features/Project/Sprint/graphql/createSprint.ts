import gql from 'graphql-tag'

export const CREATE_SPRINT = gql`
  mutation($endDate: DateTime!, $startDate: DateTime!, $title: String!, $description: String!) {
    createOneSprint(
      input: { sprint: { endDate: $endDate, startDate: $startDate, title: $title, description: $description } }
    ) {
      id
      active
      endDate
      startDate
      title
      description
    }
  }
`
