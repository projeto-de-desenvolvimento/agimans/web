import gql from 'graphql-tag'

export const ADD_CHILD_GROUPS_TO_GROUP = gql`
  mutation addChildGroupsToGroup($groupId: ID!, $childId: [ID!]!) {
    addChildGroupsToGroup(input: { id: $groupId, relationIds: $childId }) {
      id
      active
      name
      project
      slug
      description
      fatherGroup {
        active
        name
        project
        slug
        description
      }
      childGroups {
        edges {
          node {
            id
            active
            name
            project
            slug
            description
          }
        }
      }
    }
  }
`
