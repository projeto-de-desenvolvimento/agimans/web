import gql from 'graphql-tag'

export const SET_FATHER_GROUP_ON_GROUP = gql`
  mutation setFatherGroupOnGroup($groupId: ID!, $fatherId: ID!) {
    setFatherGroupOnGroup(input: { id: $groupId, relationId: $fatherId }) {
      id
      active
      name
      project
      slug
      description
      fatherGroup {
        active
        name
        project
        slug
        description
      }
      childGroups {
        edges {
          node {
            active
            name
            project
            slug
            description
          }
        }
      }
    }
  }
`
