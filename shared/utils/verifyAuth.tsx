interface AgimansToken {
  date: string
  email: string
  name: string
  token: string
}

export function setAgimansToken(token: string, email: string, name: string): void {
  const agimansToken = {
    token: token,
    email: email,
    name: name,
    date: new Date(),
  }

  localStorage.setItem('agimans_token', JSON.stringify(agimansToken))
}

export function getAgimansToken(): AgimansToken {
  return JSON.parse(window.localStorage.getItem('agimans_token'))
}

export function agimansTokenExists(): boolean {
  const agimansToken = window.localStorage.getItem('agimans_token')

  if (!agimansToken) return false
  return true
}

export function removeAgimansToken(): void {
  localStorage.removeItem('agimans_token')
}
