import { destroyCookie, parseCookies, setCookie } from 'nookies'
import { NextPageContext } from 'next'

type UserCookie = {
  token: string
  email: string
  name: string
}

export interface AgimansCookie {
  userCookie: UserCookie
  maxAge: number
  path: string
}

const AGIMANS_COOKIE = 'AgimansCookie'

export function getAgimansCookie(ctx: NextPageContext): [AgimansCookie, boolean] {
  const cookies = parseCookies(ctx.ctx)
  const agimansCookie = cookies[AGIMANS_COOKIE] ? JSON.parse(cookies[AGIMANS_COOKIE]) : ''
  const agimansCookieExists = isValidAgimansCookie(agimansCookie)

  return [agimansCookie, agimansCookieExists]
}

export function removeAgimansCookie(ctx: NextPageContext = null): void {
  destroyCookie(ctx, AGIMANS_COOKIE)
}

export function setAgimansCookie(userCookie: UserCookie, maxAge: number = 30 * 24 * 60 * 60, path = '/'): void {
  setCookie(null, AGIMANS_COOKIE, JSON.stringify({ userCookie: userCookie }), {
    maxAge: maxAge,
    path: path,
  })
}

export function isValidAgimansCookie(agimansCookie: AgimansCookie): boolean {
  return agimansCookie?.userCookie?.token?.length > 0
}
