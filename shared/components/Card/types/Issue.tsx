import React, { useState } from 'react'
import styled from 'styled-components'

import { CardContainer } from '../../common/styled'

interface Props {
  id?: number
  description?: string
  title?: string
  onClick?: () => void
}

export default function CardIssue(props: Props) {
  const [showModal, setShowModal] = useState(false)

  return (
    <CardContainer className="issue" onClick={props?.onClick}>
      <a>{props.title}</a>
    </CardContainer>
  )
}
