import React, { useRef, useContext } from 'react'
import Link from 'next/link'
import moment from 'moment'

import { useDrag, useDrop } from 'react-dnd'

import BoardContext from '../../../pages/project/[slugProject]/board/context'
import CardIssue from './types/Issue'

import { CardContainer, CardStatus, ListButton } from '../common/styled'

interface Props {
  id?: number
  description?: string
  estimate?: number
  spend?: number
  weight?: number
  slug?: string
  title?: string
  type: string
  endDate?: Date
  startDate?: Date
  color?: string
  index?: number
  lastIndex?: number
  listIndex?: number
  callFunction?: () => void
}

export interface ChildCard {
  node: Props
}

export const cardTypes = {
  group: 'group',
  groupOrProject: 'groupOrProject',
  issue: 'issue',
  list: 'list',
  project: 'project',
  sprint: 'sprint',
  status: 'status',
}

const Card = (props: Props) => {
  const index = props.index
  const listIndex = props.listIndex

  const ref = useRef()
  const { moveToCard } = useContext(BoardContext)

  const [{ isDragging }, dragRef] = useDrag({
    item: { type: 'CARD', index, listIndex },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  const [, dropRef] = useDrop({
    accept: 'CARD',
    hover(item, monitor) {
      const draggedListIndex = item.listIndex
      const targetListIndex = props.listIndex

      const draggedIndex = item.index
      const targetIndex = props.index

      if (draggedIndex === targetIndex && draggedListIndex === targetListIndex) return

      const targetSize = ref.current.getBoundingClientRect()
      const targetCenter = (targetSize.bottom - targetSize.top) / 2

      const draggedOffset = monitor.getClientOffset()
      const draggedTop = draggedOffset.y - targetSize.top

      if (draggedIndex < targetIndex && draggedTop < targetCenter) return
      if (draggedIndex > targetIndex && draggedTop > targetCenter) return

      moveToCard(draggedListIndex, targetListIndex, draggedIndex, targetIndex)

      item.index = targetIndex
      item.listIndex = targetListIndex
    },
  })

  dragRef(dropRef(ref))

  function getCard() {
    switch (props.type) {
      case cardTypes.group:
        return getGroup()

      case cardTypes.issue:
        return getIssue()

      case cardTypes.list:
        return getList()

      case cardTypes.project:
        return getProject()

      case cardTypes.sprint:
        return getSprint()

      case cardTypes.status:
        return getStatus()

      default:
        break
    }
  }

  function getGroup() {
    return (
      <CardContainer className="group">
        <Link href={`/group/[slugGroup]`} as={`/group/${props.slug}`}>
          <a>{props.title}</a>
        </Link>
        <p>{props.description}</p>
      </CardContainer>
    )
  }

  function getIssue() {
    return (
      <div ref={ref}>
        <CardIssue id={props.id} title={props.title} description={props.description} onClick={props?.callFunction} />
      </div>
    )
  }

  function getList() {
    return (
      <CardContainer className="list" color={props.color}>
        <p>{props.title}</p>
        {props.callFunction && (
          <ListButton color={props.color} onClick={props.callFunction}>
            <img src="/assets/icons/button-plus.svg" />
          </ListButton>
        )}
      </CardContainer>
    )
  }

  function getProject() {
    return (
      <CardContainer className="group">
        <Link href={`/project/[slugProject]`} as={`/project/${props.slug}`}>
          <a>{props.title}</a>
        </Link>
        <p>{props.description}</p>
      </CardContainer>
    )
  }

  function getSprint() {
    return (
      <CardContainer className="sprint">
        <div className="status">
          <h3>{props.title}</h3>
        </div>
        <div className="dates">
          <p>
            <span>Início:</span> {moment(props.startDate).format('DD/MM/YYYY hh:mm')}
          </p>
          <p>
            <span>Final:</span> {moment(props.endDate).format('DD/MM/YYYY hh:mm')}
          </p>
        </div>
      </CardContainer>
    )
  }

  function getStatus() {
    return (
      <CardStatus color={props.color}>
        <p>{props.title}</p>
      </CardStatus>
    )
  }

  return <>{getCard()}</>
}

export default Card
