import styled from 'styled-components'

export const IssueContent = styled.div`
  padding: 67px;
  width: 509px;
`

export const MarkdownContent = styled.div`
  background-color: ${({ theme }) => theme.inputBackground};
  min-width: 360px;
  max-width: 360px;
  min-height: 150px;
  border-radius: 5px;
  border: none;
  padding: 5px 10px;
  overflow-y: hidden;

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  ul,
  li {
    text-align: left;
    color: #333333;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-weight: bold;
  }

  h1 {
    font-size: 2em;
    margin-bottom: 20px;
  }

  h2 {
    font-size: 1.75em;
    margin-bottom: 17.5px;
  }

  h3 {
    font-size: 1.5em;
    margin-bottom: 15px;
  }

  h4 {
    font-size: 1.4em;
    margin-bottom: 14px;
  }

  h5 {
    font-size: 1.3em;
    margin-bottom: 13px;
  }

  h6 {
    font-size: 1.2em;
    margin-bottom: 12px;
  }

  p {
    margin-bottom: 10px;
  }

  ul {
    padding-left: 35px;
    margin-bottom: 10px;
  }

  li {
    margin-bottom: 5px;
  }
`
