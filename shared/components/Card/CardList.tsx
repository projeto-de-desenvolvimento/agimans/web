import React from 'react'
import styled from 'styled-components'

import Card, { cardTypes } from './Card'

export const CardsContainer = styled.div`
  padding: 10px;
  width: 100%;
  overflow-y: auto;
  /* height: calc(100vh - 100px); */

  &.grid-cards {
    display: inline-grid;
    grid-template-columns: auto auto;
  }

  &.status {
    width: 150px;
  }
`
interface CardNode {
  id: number
  description: string
  title?: string
  name?: string
  slug: string
  endDate?: Date
  startDate?: Date
  color?: string
  project?: boolean
}

interface CardInterface {
  node: CardNode
}

interface Props {
  cards: CardInterface[]
  type: string
  className?: string
}

function CardList({ cards, type, className }: Props) {
  function getType(project: boolean): string {
    if (project) return cardTypes.project
    return cardTypes.group
  }

  return (
    <CardsContainer className={className}>
      {cards?.map((card) => (
        <Card
          key={card.node.id}
          description={card.node.description}
          title={card.node?.title ? card.node.title : card.node.name}
          slug={card.node.slug}
          endDate={card.node?.endDate}
          startDate={card.node?.startDate}
          color={card.node?.color}
          type={cardTypes.groupOrProject === type ? getType(card.node.project) : cardTypes[type]}
        />
      ))}
    </CardsContainer>
  )
}

export default CardList
