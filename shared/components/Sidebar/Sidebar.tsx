import React, { ReactNode } from 'react'
import styled from 'styled-components'

import { NextPageContext } from 'next'

import Link from 'next/link'

import { removeAgimansCookie } from '../../utils/cookies'

const SidebarContainer = styled.nav`
  grid-area: sidebar;
  background-color: ${({ theme }) => theme.primary};
  background: ${({ theme }) => theme.gradient.primaryVertical};
  background-blend-mode: multiply;
  padding: 5px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  img {
    width: 100%;
  }

  header {
  }

  section {
  }

  footer {
    padding: 5px;
  }
`
interface Props {
  children: ReactNode
  ctx?: NextPageContext
}

function Sidebar(props: Props) {
  function logout() {
    removeAgimansCookie()
  }

  return (
    <SidebarContainer>
      <header>
        <Link href="/">
          <a>
            <img src="/assets/icons/logo-agimans.svg" alt="" />
          </a>
        </Link>
      </header>
      <section>{props.children}</section>
      <footer>
        <Link href="/signin">
          <a onClick={() => logout()}>
            <img src="/assets/icons/signout.svg" alt="" />
          </a>
        </Link>
      </footer>
    </SidebarContainer>
  )
}

export default Sidebar
