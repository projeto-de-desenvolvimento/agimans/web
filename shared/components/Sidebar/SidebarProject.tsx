import React from 'react'
import styled from 'styled-components'

import Icon from '../common/Icon'

const IconContainer = styled.div`
  div + div {
    margin-top: 37px;
  }
`

interface Props {
  slugProject: string
}

const SidebarProject = (props: Props) => {
  const icons = [
    {
      imgPath: '/assets/icons/board.svg',
      text: 'Board',
      path: `/project/[slugProject]/board`,
      as: `/project/${props.slugProject}/board`,
    },
    {
      imgPath: '/assets/icons/progress.svg',
      text: 'Progresso',
      path: `/project/[slugProject]/progress`,
      as: `/project/${props.slugProject}/progress`,
    },
  ]

  return (
    <>
      <IconContainer>
        {icons.map((icon, index) => (
          <Icon key={index} imgPath={icon.imgPath} text={icon.text} path={icon.path} as={icon.as} />
        ))}
      </IconContainer>
    </>
  )
}

export default SidebarProject
