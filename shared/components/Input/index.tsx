import React, { ChangeEvent, useState } from 'react'

import { StyledInput, StyledInputContainer } from './styles'

export interface InputInterface {
  id?: string
  name: string
  type: string
  autoComplete?: string
  required?: boolean
  placeholder?: string
  label?: string
  options?: any
  defaultValue?: string | number | string[]
  onChange?: (event: ChangeEvent<HTMLElement>) => void
}

interface Props {
  input: InputInterface
}

export default function Input(props: Props) {
  const [inputContent, setInputContent] = useState(props?.input?.defaultValue)

  function handleChange(event) {
    setInputContent(event.target.value)
  }

  function getInputType(input: InputInterface) {
    switch (input.type) {
      case 'checkbox':
        return (
          <div className="checkbox">
            <span>{input.placeholder}</span>
            <input id="project" name="project" type="checkbox" />
          </div>
        )

      case 'select':
        return (
          <select name={input.name} className="select">
            <option value="0" defaultValue="Status" disabled>
              {input.placeholder}
            </option>
            {input?.options?.map((option) => (
              <option key={option.node.id} value={option.node.id}>
                {option.node.name}
              </option>
            ))}
          </select>
        )

      case 'textarea':
        return <textarea name={input.name} value={inputContent} onChange={handleChange} cols={30} rows={10}></textarea>

      default:
        return (
          <StyledInput
            autoComplete={input.autoComplete}
            id={[input.name, 'input'].join('-')}
            name={input.name}
            placeholder={input.placeholder}
            required={input.required}
            type={input.type}
            value={inputContent}
            onChange={handleChange}
          />
        )
    }
  }

  return (
    <StyledInputContainer>
      {props?.input?.label && (
        <div className="date-time">
          <p>{props.input.label}</p>
        </div>
      )}
      {props?.input && getInputType(props.input)}
    </StyledInputContainer>
  )
}
