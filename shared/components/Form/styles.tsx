import styled from 'styled-components'

export const FormContainer = styled.div`
  grid-area: 'form';
  height: calc(100vh - 100px);
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ theme }) => theme.gradient.primaryForm};
  background-blend-mode: multiply;

  h2 {
    text-align: center;
    font-weight: 400;
    color: ${({ theme }) => theme.font.light};
  }

  form {
    padding: 67px;
    width: 509px;

    .checkbox {
      padding: 5px 0px;
      font-size: 15px;
      color: ${({ theme }) => theme.font.light};
      display: flex;
      align-items: center;

      span {
        margin-right: 10px;
      }

      input {
        width: 17px;
        height: 17px;
      }
    }
  }
`

export const FormButtonContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const FormButton = styled.button`
  background-color: ${({ theme }) => theme.secondary};
  color: ${({ theme }) => theme.font.light};
  width: 167px;
  height: 40px;
  margin: 10px;
  border: none;
  border-radius: 5px;
  font-size: 15px;
  cursor: pointer;

  &:hover {
    background: ${({ theme }) => theme.gradient.secondary};
    background-blend-mode: multiply;
  }

  &.modal-issue-button {
    margin-left: 0;
  }

  &.modal-issue-button + button.modal-issue-button {
    margin-left: 16px;
  }

  &.full-width {
    width: calc(100% - 16px);
    margin-left: 0;
  }
`

export const ContentForm = styled.div`
  grid-area: 'form';
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const SignInButtonContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const SignInButton = styled.button`
  background-color: ${({ theme }) => theme.componentBackground};
  color: ${({ theme }) => theme.font.medium};
  width: 170px;
  height: 40px;
  margin: 10px;
  border: none;
  border-radius: 5px;
  font-size: 15px;
  cursor: pointer;

  &:hover {
    background: ${({ theme }) => theme.gradient.grayScale};
    background-blend-mode: multiply;
  }
`

export const LinkButton = styled.a`
  background-color: ${({ theme }) => theme.componentBackground};
  color: ${({ theme }) => theme.font.medium};
  width: 170px;
  height: 40px;
  margin: 10px;
  border-radius: 5px;
  text-decoration: none;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 15px;
  cursor: pointer;

  &:hover {
    background: ${({ theme }) => theme.gradient.grayScale};
    background-blend-mode: multiply;
  }
`
