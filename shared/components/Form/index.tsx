import React, { FormEvent, useEffect } from 'react'

import Input, { InputInterface } from '../Input'

import {
  ContentForm,
  FormButton,
  FormButtonContainer,
  FormContainer,
  LinkButton,
  SignInButton,
  SignInButtonContainer,
} from './styles'

export interface ButtonFormInterface {
  name: string
  type: any
  onClick?: any
}

export interface LinkButtonFormInterface {
  name: string
  href?: string
  onClick?: any
}

interface Props {
  title?: string
  subtitle?: string
  signForm?: boolean
  hide?: boolean
  inputs: InputInterface[]
  buttons: ButtonFormInterface[]
  linkButtons?: LinkButtonFormInterface[]
  handleSubmit: (event: FormEvent<HTMLFormElement>) => void
}

export const buttonTypes = {
  button: 'button',
  submit: 'submit',
  reset: 'reset',
}

export let formHTMLElement: any

const Form = (props: Props) => {
  useEffect(() => {
    formHTMLElement = document.getElementById('form') as HTMLElement
  }, [])

  return (
    <>
      {props.signForm && !props.hide && (
        <ContentForm>
          <form id="form" onSubmit={props.handleSubmit}>
            {props?.inputs?.map((input: InputInterface, index: number) => (
              <Input key={index} input={input} />
            ))}
            <SignInButtonContainer>
              {props?.buttons?.map((button: ButtonFormInterface, index: number) => (
                <SignInButton key={index} type={button.type} onClick={button.onClick ? button.onClick : null}>
                  {button.name}
                </SignInButton>
              ))}
              {props?.linkButtons?.map((linkButton: LinkButtonFormInterface, index: number) => (
                <LinkButton
                  key={index}
                  href={linkButton.href ? linkButton.href : null}
                  onClick={linkButton.onClick ? linkButton.onClick : null}
                >
                  {linkButton.name}
                </LinkButton>
              ))}
            </SignInButtonContainer>
          </form>
        </ContentForm>
      )}

      {!props.signForm && !props.hide && (
        <FormContainer>
          <div>
            {props.title && <h2>{props.title}</h2>}
            {props.subtitle && <h2>{props.subtitle}</h2>}
            <form id="form" onSubmit={props.handleSubmit}>
              {props?.inputs?.map((input: InputInterface, index: number) => (
                <Input key={index} input={input} />
              ))}
              <FormButtonContainer>
                {props?.buttons?.map((button: ButtonFormInterface, index: number) => (
                  <FormButton key={index} type={button.type} onClick={button.onClick ? button.onClick : null}>
                    {button.name}
                  </FormButton>
                ))}
              </FormButtonContainer>
            </form>
          </div>
        </FormContainer>
      )}
    </>
  )
}

export default Form
