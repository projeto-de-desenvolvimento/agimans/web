import React from 'react'
import styled from 'styled-components'

const HeaderContainer = styled.header`
  grid-area: header;
  background-color: ${({ theme }) => theme.primary};
  background: ${({ theme }) => theme.gradient.primaryHorizontal};
  background-blend-mode: multiply;
`

function Header() {
  return <HeaderContainer></HeaderContainer>
}

export default Header
