import React, { useContext } from 'react'
import styled from 'styled-components'

import { DragObjectWithType, useDrop } from 'react-dnd'

import BoardContext from '../../../pages/project/[slugProject]/board/context'
import Card, { cardTypes, ChildCard } from '../Card/Card'

const ListContainer = styled.div`
  margin: 0px 10px 10px 10px;
  width: 350px;
  height: calc(100% - 50px);
  flex: 0 0 350px;
  overflow: none;

  & + div {
    border-left: 1px solid ${({ theme }) => theme.dark10};
  }
`

interface Props {
  name?: string
  childCards?: []
  index?: number
  statusColor?: string
  callFunction?: () => void
  callFuntionToCards?: ({
    id,
    title,
    description,
    estimate,
    spend,
    weight,
  }: {
    id: number
    title: string
    description: string
    estimate: number
    spend: number
    weight: number
  }) => void
}

interface Index extends DragObjectWithType {
  index: number
  listIndex: number
}

function List(props: Props) {
  let lastIndex = 0
  const { moveToList } = useContext(BoardContext)

  const [, dropRef] = useDrop({
    accept: 'CARD',
    hover(item: Index) {
      const draggedListIndex = Number(item.listIndex)
      const targetListIndex = props.index

      const draggedIndex = item.index
      const targetIndex = lastIndex

      if (draggedListIndex === targetListIndex) return

      moveToList(draggedListIndex, targetListIndex, draggedIndex, targetIndex)

      item.index = targetIndex
      item.listIndex = targetListIndex
    },
  })

  function listCards(childCard: ChildCard, index: number) {
    lastIndex = index + 1

    return (
      childCard?.node?.id && (
        <Card
          key={childCard.node.id}
          title={childCard.node.title}
          index={index}
          description={childCard.node.description}
          type={cardTypes.issue}
          listIndex={props.index}
          callFunction={() =>
            props?.callFuntionToCards({
              id: childCard.node.id,
              title: childCard.node.title,
              description: childCard.node.description,
              estimate: childCard.node.estimate,
              spend: childCard.node.spend,
              weight: childCard.node.weight,
            })
          }
        />
      )
    )
  }

  return (
    <ListContainer ref={dropRef}>
      <Card title={props.name} type={cardTypes.list} color={props.statusColor} callFunction={props.callFunction} />
      {props.childCards.map((childCard, index) => listCards(childCard, index))}
    </ListContainer>
  )
}

export default List
