import React, { ReactNode } from 'react'
import styled from 'styled-components'

const SubHeaderContainer = styled.header`
  background-color: ${({ theme }) => theme.secondary};
  height: 50px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row;

  a {
    cursor: pointer;
  }

  a:first-child:before {
    content: url('/assets/icons/sub-header.svg');
  }

  a:before {
    content: url('/assets/icons/sub-header-link.svg');
    cursor: auto;
    margin: 17.5px;
  }
`

interface Props {
  children: ReactNode
}

const SubHeader = ({ children }: Props) => {
  return <SubHeaderContainer>{children}</SubHeaderContainer>
}

export default SubHeader
