import React, { ReactNode } from 'react'
import styled from 'styled-components'

const ModalContainer = styled.div`
  height: calc(100% - 50px);
  max-width: 600px;
  position: absolute;
  right: 0;
  top: 50px;
  z-index: 10;
  background-color: ${({ theme }) => theme.light};

  &.modal-card {
    top: -70px;
    height: calc(100% + 70px);
  }
`

interface Props {
  children?: ReactNode
  showModal?: boolean
  className?: string
}
export default function Modal(props: Props) {
  return props.showModal && <ModalContainer className={props?.className}>{props.children}</ModalContainer>
}
