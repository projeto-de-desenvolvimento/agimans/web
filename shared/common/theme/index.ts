import { ThemeInterface, ThemeType } from './types'

export let theme: ThemeType

const themes: ThemeInterface = {
  light: {
    primary: '#226699',
    primary75: 'RGBA(34, 102, 153, 0.75)',
    primary50: 'RGBA(34, 102, 153, 0.5)',
    secondary: '#119999',
    tertiary: '#53006C',
    light: '#FFFFFF',
    dark: '#000000',
    dark10: 'RGBA(0, 0, 0, 0.1)',
    mainBackground: '#E5E5E5',
    componentBackground: '#F0F0F0',
    inputBackground: 'RGBA(229, 229, 229, 0.85)',
    shadow: 'RGBA(192, 208, 230, 0.8)',
    gradient: {
      primaryVertical: `radial-gradient(150% 75% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(34, 102, 153, 0.5) 100%), rgba(34, 102, 153, 1)`,
      primaryHorizontal: `radial-gradient(75% 150% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(34, 102, 153, 0.5) 100%), rgba(34, 102, 153, 1)`,
      primaryForm: `radial-gradient(59.21% 59.21% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(34, 102, 153, 0.5) 100%), rgba(34, 102, 153, 0.75)`,
      secondary: `radial-gradient(65% 55% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(17, 153, 153, 0.5) 100%), rgba(17, 153, 153, 1)`,
      grayScale: `radial-gradient(65% 55% at 50% 50%, rgba(255, 255, 255, 0.85) 0%, rgba(255, 255, 255, 0.65) 100%), rgba(255, 255, 255, 0.25)`,
      darkScale: `radial-gradient(59.21% 59.21% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(0, 17, 51, 0.25) 100%), rgba(0, 17, 51, 0.25)`,
    },
    default: '#001133',
    font: {
      primary: '#226699',
      light: '#FFFFFF',
      lightLess: '#DDDDDD',
      medium: '#777777',
      darkLess: '#333333',
      dark: '000000',
    },
  },
  dark: {
    primary: '#119999',
    primary75: 'RGBA(34, 102, 153, 0.75)',
    primary50: 'RGBA(34, 102, 153, 0.5)',
    secondary: '#A014CA',
    tertiary: '#3399BB',
    light: '#FFFFFF',
    dark: '#000000',
    dark10: 'RGBA(0, 0, 0, 0.1)',
    mainBackground: '#353535',
    componentBackground: '#F0F0F0',
    inputBackground: 'RGBA(229, 229, 229, 0.85)',
    shadow: 'RGBA(192, 208, 230, 0.8)',
    gradient: {
      primaryVertical: `radial-gradient(150% 75% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(34, 102, 153, 0.5) 100%), rgba(34, 102, 153, 1)`,
      primaryHorizontal: `radial-gradient(75% 150% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(34, 102, 153, 0.5) 100%), rgba(34, 102, 153, 1)`,
      primaryForm: `radial-gradient(59.21% 59.21% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(34, 102, 153, 0.5) 100%), rgba(34, 102, 153, 0.75)`,
      secondary: `radial-gradient(65% 55% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(17, 153, 153, 0.5) 100%), rgba(17, 153, 153, 1)`,
      grayScale: `radial-gradient(65% 55% at 50% 50%, rgba(255, 255, 255, 0.85) 0%, rgba(255, 255, 255, 0.65) 100%), rgba(255, 255, 255, 0.25)`,
      darkScale: `radial-gradient(59.21% 59.21% at 50% 50%, rgba(255, 255, 255, 0) 0%, rgba(0, 17, 51, 0.25) 100%), rgba(0, 17, 51, 0.25)`,
    },
    default: '#001133',
    font: {
      primary: '#226699',
      light: '#FFFFFF',
      lightLess: '#DDDDDD',
      medium: '#777777',
      darkLess: '#333333',
      dark: '000000',
    },
  },
}

export function setTheme(): ThemeType {
  theme = themes['light']
  return theme
}

export default theme
